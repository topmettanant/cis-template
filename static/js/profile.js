/**
 * Created by vacharapat on 7/6/2015 AD.
 */

$(document).ready(function(){
  $.ajax({
    type: 'GET',
    url: '/setting/general_profile/',
    data: { get_param: 'value' },
    dataType: 'json',
    error: function(xhr, textStatus, errorThrown){
       alert('request failed');
    },
    success: function(data){
      var items = [];
      items.push({label:"อีเมล", type:"text", id:"email", value: data['email']});
      items.push({label:"ชื่อจริง(ไทย)", type:"text", id:"first_name_th", value: data['first_name_th'], validate:"isEmail"});
      items.push({label:"นามสกุล(ไทย)", type:"text", id:"last_name_th", value: data['last_name_th']});
      items.push({label:"หมายเลขภายใน", type:"text", id:"internal_number", value: data['internal_number']});
      items.push({label:"โทรศัพท์มือถือ", type:"text", id:"mobile_phone_number", value: data['mobile_phone_number']});
      var profileView = {
        view:"property",
        id:"profile-view",
        editable:false,
        nameWidth: 150,
        autoheight:true,
        elements:items,
      };
      $$("body").addView(profileView, 2);
    }
  });
});
