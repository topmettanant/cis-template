from django.shortcuts import render
from django.contrib.auth.decorators import login_required
import urllib.request, urllib.parse
from django.http import HttpResponse, HttpResponseRedirect
from user_profile.models import UserDetails
from students.models import *
import json
import re
import datetime

def student_data_url():
    return "http://158.108.97.15:5000"

def updating_days():
    return 1

def get_user_details(request):
    user = request.user
    user_details = UserDetails.objects.get(first_name=user.first_name, last_name=user.last_name)
    return user_details

def get_advisor_from(user_details):
    try:
        advisor = Advisor.objects.get(user_details=user_details)
        return advisor
    except:
        url = student_data_url() + '/lecturers/'
        try:
            lecturers = json.loads(urllib.request.urlopen(url).readall().decode('utf-8'))['lecturers']
        except:
            lecturers = []
        me = None
        english_name_pattern = re.compile("^[a-zA-Z]+ +[a-zA-Z]+$")
        for lecturer in lecturers:
            name = lecturer['name'].title().strip()
            if english_name_pattern.match(name):
                if name.startswith(user_details.first_name) and name.endswith(user_details.last_name):
                    me = lecturer
            else:
                if name.startswith(user_details.first_name_th) and name.endswith(user_details.last_name_th):
                    me = lecturer
        if me != None:
            advisor = Advisor.objects.create(advisor_id=me['id'],user_details=user_details)
            advisor.save()
            #get_all_students_of(advisor)
            return advisor
        else:
            return None


def get_all_students_of(advisor):
    if advisor.last_update == None or (datetime.date.today() - advisor.last_update).days >= updating_days():
        url = student_data_url() + '/students/?fields=first_name_th,last_name_th,first_name_en,last_name_en,'
        url += 'gender,birth_date,birth_place,blood,gpa,school_gpa,school,year,status,personal_id,major'
        url += '&advisor.id=' + advisor.advisor_id
        students = []
        try:
            result = json.loads(urllib.request.urlopen(url).readall().decode('utf-8'))
            students += result['students']
            while 'next' in result['paging'].keys():
                url = result['paging']['next']
                result = json.loads(urllib.request.urlopen(url).readall().decode('utf-8'))
                students += result['students']
        except:
            students = []
        for student in students:
            try:
                s = Student.objects.get_or_create(student_id=student['id'],advisor=advisor)[0]
                s.major_id=student['major']['id']
                s.first_name_en=student['first_name_en']
                s.last_name_en=student['last_name_en']
                s.first_name_th = student['first_name_th']
                s.last_name_th = student['last_name_th']
                s.year=student['year']
                s.status = student['status']
                s.gpa=student['gpa']
                ps = Program.objects.filter(program_id=s.major_id,year__lte=s.year)
                if len(ps)>0:
                    program = ps[0]
                    for p in ps:
                        if p.year > program.year:
                            program = p
                    s.program = p

                s.personal_id = student['personal_id']
                s.gender = student['gender']
                s.blood = student['blood']
                s.birth_date = student['birth_date']['date']
                s.birth_month = student['birth_date']['month']
                s.birth_year = student['birth_date']['year']
                s.birth_province = student['birth_place']['province']

                s.school_gpa = student['school_gpa']
                s.school_province = student['school']['province']
                s.school_id = student['school']['id']
                s.school_name_th = student['school']['name_th']
                s.school_name_en = student['school']['name_en']

                s.save()
            except:
                pass
        advisor.last_update = datetime.date.today()
        advisor.save()
    return Student.objects.filter(advisor=advisor)

def get_registrations_of(student):
    if student.last_update == None or (datetime.date.today() - student.last_update).days >= updating_days():
        courses = {}
        if student.program != None:
            for course_group in student.program.coursegroup_set.all():
                for course in course_group.courses.all():
                    courses[course.course_id]=course
        url = student_data_url() + '/registrations/?student.id=' + student.student_id
        regs = []
        try:
            result = json.loads(urllib.request.urlopen(url).readall().decode('utf-8'))
            regs += result['registrations']
            while 'next' in result['paging'].keys():
                url = result['paging']['next']
                result = json.loads(urllib.request.urlopen(url).readall().decode('utf-8'))
                regs += result['registrations']
        except:
            pass

        for reg in regs:
            if reg['course']['id'] in courses.keys():
                course = courses[reg['course']['id']]
            else:
                course, created = Course.objects.get_or_create(course_id=reg['course']['id'])
                if created:
                    url = student_data_url() + '/courses/' + reg['course']['id']
                    result = json.loads(urllib.request.urlopen(url).readall().decode('utf-8'))
                    course.name_en = result['name_en']
                    course.name_th = result['name_th']
                    course.unit_lecture = result['unit_lecture']
                    course.unit_lab = result['unit_lab']
                    course.prerequisite = []
                    course.save()
            r = Registration.objects.get_or_create(student=student, course=course,semester=reg['semester'],year=reg['year'])[0]
            if reg['section'] != None:
                r.section = int(reg['section'])
            r.registration_type = reg['type']
            r.grade = reg['grade']
            r.save()
        student.last_update = datetime.date.today()
        student.save()
    regs = Registration.objects.filter(student=student)
    return regs


@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def index(request):
    user_details = get_user_details(request)
    if not user_details.students_info:
        return HttpResponseRedirect('/')
    context = {'request': request, 'user_details': user_details}
    return render(request, 'students/index.html', context)

@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_gpa_sum_json(request):
    user_details = get_user_details(request)
    advisor = get_advisor_from(user_details)
    students = get_all_students_of(advisor)
    active_students = [s for s in students if s.status=='normal' or s.status=='back']
    leave_students = [s for s in students if s.status=='leave']


    data = {"leave":0, "0":0,"1":0, "2":0, "3":0, "4":0, "5":0, 'sum':0}
    for student in active_students:
        if student.gpa < 1.5:
            data['0'] += 1
        elif student.gpa < 1.75:
            data['1'] += 1
        elif student.gpa < 2:
            data['2'] += 1
        elif student.gpa < 3.25:
            data['3'] += 1
        elif student.gpa < 3.5:
            data['4'] += 1
        else:
            data['5'] += 1
        data['sum'] += 1
    for student in leave_students:
        data['leave'] += 1
        data['sum'] += 1

    return HttpResponse(json.dumps(data), content_type="application/json")

@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_gpa_diff_json(request):
    user_details = get_user_details(request)
    advisor = get_advisor_from(user_details)
    students = get_all_students_of(advisor)
    active_students = [s for s in students if s.status=='normal' or s.status=='back']
    data = []
    last_year = 0
    last_term = 0
    for student in active_students:
        regs = get_registrations_of(student)
        sum_grades = 0
        sum_credits = 0
        for reg in regs:
            credit = reg.course.unit_lab + reg.course.unit_lecture
            if reg.grade in ['A','B+','B','C+','C','D+','D','F']:
                sum_credits += credit
            if reg.grade == 'A':
                sum_grades += 4 * credit
            elif reg.grade == 'B+':
                sum_grades += 3.5 * credit
            elif reg.grade == 'B':
                sum_grades += 3 * credit
            elif reg.grade == 'C+':
                sum_grades += 2.5 * credit
            elif reg.grade == 'C':
                sum_grades += 2 * credit
            elif reg.grade == 'D+':
                sum_grades += 1.5 * credit
            elif reg.grade == 'D':
                sum_grades += 1 * credit

            if reg.year > last_year:
                last_year = reg.year
                last_term = reg.semester
            elif reg.year == last_year and reg.semester > last_term:
                last_term = reg.semester
        gpa_new = sum_grades/sum_credits
        student.gpa = gpa_new
        student.save()

        sum_grades = 0
        sum_credits = 0
        for reg in regs:
            if not (reg.year == last_year and reg.semester == last_term):
                credit = reg.course.unit_lab + reg.course.unit_lecture
                if reg.grade in ['A','B+','B','C+','C','D+','D','F']:
                    sum_credits += credit
                if reg.grade == 'A':
                    sum_grades += 4 * credit
                elif reg.grade == 'B+':
                    sum_grades += 3.5 * credit
                elif reg.grade == 'B':
                    sum_grades += 3 * credit
                elif reg.grade == 'C+':
                    sum_grades += 2.5 * credit
                elif reg.grade == 'C':
                    sum_grades += 2 * credit
                elif reg.grade == 'D+':
                    sum_grades += 1.5 * credit
                elif reg.grade == 'D':
                    sum_grades += 1 * credit
        gpa_old = sum_grades/sum_credits

        data.append({'student_id':student.student_id, 'gpa_old':gpa_old, 'gpa_new':gpa_new,'color':'green' if gpa_new >= gpa_old else 'red'})
    return HttpResponse(json.dumps(data), content_type="application/json")

@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_unit_progress_json(request):
    user_details = get_user_details(request)
    advisor = get_advisor_from(user_details)
    students = get_all_students_of(advisor)
    active_students = [s for s in students if s.status=='normal' or s.status=='back']
    last_year = 0
    last_term = 0
    for student in active_students:
        regs = get_registrations_of(student)
        for reg in regs:
            if reg.year > last_year:
                last_year = reg.year
                last_term = reg.semester
            elif reg.year == last_year and reg.semester > last_term:
                last_term = reg.semester
    unit_required = {}
    data = {}
    for student in active_students:
        if student.program != None:
            if not ((student.program, student.year) in unit_required.keys()):
                std_year = last_year - student.year + 1
                if last_term == 0:
                    std_year -= 1
                    last_term = 2
                unit_count = 0
                year = 0
                term = 2
                while not (year == std_year and term == last_term) and year <= 4:
                    term += 1
                    if term > 2:
                        term = 1
                        year += 1
                    plan = ProgramPlan.objects.get(program=student.program,plan_type='normal',semester=term,year=year)
                    unit_count += plan.unit
                unit_required[(student.program, student.year)] = unit_count

        regs = get_registrations_of(student)
        unit_passed = 0
        for reg in regs:
            if reg.grade in ['A','B+','B','C+','C','D+','D','P','S']:
                unit_passed += reg.course.unit_lecture + reg.course.unit_lab
        if student.program == None:
            color = 'purple'
        elif unit_passed >= unit_required[(student.program,student.year)]:
            color = 'green'
        else:
            color = 'blue'
        if not (student.year%100 in data.keys()):
            data[student.year%100] = []
        data[student.year%100].append({'student_id':student.student_id,'gpa':student.gpa,
                        'unit_passed':unit_passed,'color':color})
    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_f_count_json(request):
    user_details = get_user_details(request)
    advisor = get_advisor_from(user_details)
    students = get_all_students_of(advisor)
    active_students = [s for s in students if s.status=='normal' or s.status=='back']
    last_year = 0
    last_term = 0
    for student in active_students:
        regs = get_registrations_of(student)
        for reg in regs:
            if reg.year > last_year:
                last_year = reg.year
                last_term = reg.semester
            elif reg.year == last_year and reg.semester > last_term:
                last_term = reg.semester

    f_all = {}
    f_last = {}
    w_all = {}
    w_last = {}
    for student in active_students:
        f_regs = student.registration_set.filter(grade='F')#,semester=last_term,year=last_year)
        for reg in f_regs:
            if reg.course in f_all.keys():
                f_all[reg.course] += 1
            else:
                f_all[reg.course] = 1
            if reg.semester==last_term and reg.year==last_year:
                if reg.course in f_last.keys():
                    f_last[reg.course] += 1
                else:
                    f_last[reg.course] = 1
        w_regs = student.registration_set.filter(grade='W')
        for reg in w_regs:
            if reg.course in w_all.keys():
                w_all[reg.course] += 1
            else:
                w_all[reg.course] = 1
            if reg.semester==last_term and reg.year == last_year:
                if reg.course in w_last.keys():
                    w_last[reg.course] += 1
                else:
                    w_last[reg.course] = 1

    max_f_all = []
    max_f_last = []
    max_w_all = []
    max_w_last = []
    max_count = 0
    for i in range(10):
        try:
            course = max(f_all, key=lambda i: f_all[i])
            max_f_all.append({'course_id':course.course_id,'course_name':course.name_en, 'count':f_all[course]})
            if f_all[course] > max_count:
                max_count = f_all[course]
            f_all.pop(course, None)
        except:
            pass

        try:
            course = max(f_last, key=lambda i: f_last[i])
            max_f_last.append({'course_id':course.course_id,'course_name':course.name_en, 'count':f_last[course]})
            if f_last[course] > max_count:
                max_count = f_last[course]
            f_last.pop(course, None)
        except:
            pass

        try:
            course = max(w_all, key=lambda i: w_all[i])
            max_w_all.append({'course_id':course.course_id,'course_name':course.name_en, 'count':w_all[course]})
            if w_all[course] > max_count:
                max_count = w_all[course]
            w_all.pop(course, None)
        except:
            pass

        try:
            course = max(w_last, key=lambda i: w_last[i])
            max_w_last.append({'course_id':course.course_id,'course_name':course.name_en, 'count':w_last[course]})
            if w_last[course] > max_count:
                max_count = w_last[course]
            w_last.pop(course, None)
        except:
            pass

    return HttpResponse(json.dumps({'max':max_count,'f_all':max_f_all, 'f_last':max_f_last,
                                    'w_all':max_w_all, 'w_last':max_w_last}), content_type="application/json")

@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_students_json(request):
    user_details = get_user_details(request)
    advisor = get_advisor_from(user_details)
    students = get_all_students_of(advisor)
    active_students = [s for s in students if s.status=='normal' or s.status=='back']
    data = []
    for student in active_students:
        regs = get_registrations_of(student)
        unit_passed = 0
        for reg in regs:
            if reg.grade in ['A','B+','B','C+','C','D+','D','P','S']:
                unit_passed += reg.course.unit_lecture + reg.course.unit_lab
        data.append({'id':student.student_id,'first_name':student.first_name_th,
                    'last_name':student.last_name_th, 'gender':"ชาย" if student.gender == 'male' else "หญิง",
                    'gpa':student.gpa,'birth_province':student.birth_province,
                    'unit_passed':unit_passed})
    return HttpResponse(json.dumps(data), content_type="application/json")
