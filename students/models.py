from django.db import models
from user_profile.models import UserDetails

# Create your models here.
class Program(models.Model):
    level = models.CharField(max_length=32)
    name_en = models.CharField(max_length=128)
    name_th = models.CharField(max_length=128)
    year = models.IntegerField(default=2555)
    program_id = models.CharField(max_length=16)

    class Meta:
        unique_together = ("year", "program_id")

    def __str__(self):
        return self.name_en + ' ' + str(self.year)


class Course(models.Model):

    course_id = models.CharField(max_length=32)
    year = models.IntegerField(default=0)
    name_en = models.CharField(max_length=64,default='')
    name_th = models.CharField(max_length=64,default='')
    unit_lecture = models.IntegerField(default=0)
    unit_lab = models.IntegerField(default=0)
    prerequisite = models.ManyToManyField("self", symmetrical=False)

    class Meta:
        unique_together = ("course_id", "year")

    def __str__(self):
        return self.name_en


class CourseGroup(models.Model):
    name = models.CharField(max_length=128)
    unit_required = models.IntegerField(default=0)
    courses = models.ManyToManyField(Course)
    program = models.ForeignKey(Program)

    def __str__(self):
        return self.name + ' ' + self.program.name_en


class Advisor(models.Model):
    advisor_id = models.CharField(max_length=32,unique=True)
    user_details = models.OneToOneField(UserDetails)
    last_update = models.DateField(null=True)

    def __str__(self):
        return str(self.user_details)

class ProgramPlan(models.Model):
    program = models.ForeignKey(Program)
    plan_type = models.CharField(max_length=32)
    semester = models.IntegerField()
    year = models.IntegerField()
    unit = models.IntegerField()

class Student(models.Model):
    advisor = models.ForeignKey(Advisor)
    major_id = models.CharField(max_length=16,default='')
    student_id = models.CharField(max_length=32,unique=True)
    first_name_en = models.CharField(max_length=32,default='')
    last_name_en = models.CharField(max_length=32,default='')
    first_name_th = models.CharField(max_length=32,default='')
    last_name_th = models.CharField(max_length=32,default='')
    year = models.IntegerField(default=0)
    status = models.CharField(max_length=16,default='')
    gpa = models.FloatField(default=0)
    program = models.ForeignKey(Program,null=True)

    personal_id = models.CharField(max_length=32,null=True)
    gender = models.CharField(max_length=16, null=True)
    blood = models.CharField(max_length=4,null=True)
    birth_date = models.IntegerField(null=True)
    birth_month = models.IntegerField(null=True)
    birth_year = models.IntegerField(null=True)
    birth_province = models.CharField(max_length=32,null=True)

    school_gpa = models.FloatField(null=True)
    school_province = models.CharField(max_length=64,null=True)
    school_id = models.CharField(max_length=32,null=True)
    school_name_th = models.CharField(max_length=64,null=True)
    school_name_en = models.CharField(max_length=64,null=True)

    last_update = models.DateField(null=True)


    def save(self, *args, **kwargs):
        self.first_name_en = self.first_name_en.title().strip()
        self.last_name_en = self.last_name_en.title().strip()
        self.first_name_th = self.first_name_th.strip()
        self.last_name_th = self.last_name_th.strip()

        if self.birth_province != None:
            if self.birth_province.strip() in ['กทม','กทม.','ก.ท.ม.','กรุงเทพ','กรุงเทพฯ','กรุงเทพ ฯ']:
                self.birth_province = 'กรุงเทพมหานคร'

        if self.school_province != None:
            if self.school_province.strip() in ['กทม','กทม.','ก.ท.ม.','กรุงเทพ','กรุงเทพฯ','กรุงเทพ ฯ']:
                self.school_province = 'กรุงเทพมหานคร'

        super(Student, self).save(*args, **kwargs)

    def __str__(self):
        return self.student_id + ' ' + self.first_name_th + ' ' + self.last_name_th

class Registration(models.Model):
    student = models.ForeignKey(Student)
    course = models.ForeignKey(Course)
    section = models.IntegerField(null=True)
    semester = models.IntegerField(null=True)
    year = models.IntegerField(null=True)
    registration_type = models.CharField(max_length=8,null=True)
    grade = models.CharField(max_length=8,default='N')

    class Meta:
        unique_together = ("student", "course", "semester", "year")

    def __str__(self):
        return self.student.student_id + ' ' + self.course.course_id
