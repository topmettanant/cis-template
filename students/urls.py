from django.conf.urls import include, url
from students import views

urlpatterns = [

    url(r'^$', views.index, name='students'),
    url(r'^get_gpa_sum_json/$', views.get_gpa_sum_json, name='students_gpa'),
    url(r'^get_gpa_diff_json/$', views.get_gpa_diff_json, name='students_gpa_diff'),
    url(r'^get_f_count_json/$', views.get_f_count_json, name='students_f_count_last_semester'),
    url(r'^get_unit_progress_json/$', views.get_unit_progress_json, name='students_unit_progress'),
    url(r'^get_students_json/$', views.get_students_json, name='students_datatable'),
]
