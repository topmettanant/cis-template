# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0008_auto_20160618_1515'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='school_gpa',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='school_id',
            field=models.CharField(null=True, max_length=32),
        ),
        migrations.AlterField(
            model_name='student',
            name='school_name_en',
            field=models.CharField(null=True, max_length=64),
        ),
        migrations.AlterField(
            model_name='student',
            name='school_name_th',
            field=models.CharField(null=True, max_length=64),
        ),
        migrations.AlterField(
            model_name='student',
            name='school_province',
            field=models.CharField(null=True, max_length=64),
        ),
    ]
