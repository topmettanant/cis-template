# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0013_auto_20160620_1631'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProgramPlan',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('plan_type', models.CharField(max_length=32)),
                ('semester', models.IntegerField()),
                ('year', models.IntegerField()),
                ('unit', models.IntegerField()),
                ('program', models.ForeignKey(to='students.Program')),
            ],
        ),
    ]
