# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='unit_lab',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='course',
            name='unit_lecture',
            field=models.IntegerField(default=0),
        ),
    ]
