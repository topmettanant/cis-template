# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0004_userdetails_students_info'),
        ('students', '0006_auto_20160617_1120'),
    ]

    operations = [
        migrations.CreateModel(
            name='Advisor',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('advisor_id', models.CharField(max_length=32)),
                ('last_update', models.DateField(blank=True)),
                ('user_details', models.OneToOneField(to='user_profile.UserDetails')),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('major_id', models.CharField(max_length=16)),
                ('student_id', models.CharField(max_length=32)),
                ('first_name_en', models.CharField(max_length=32)),
                ('last_name_en', models.CharField(max_length=32)),
                ('first_name_th', models.CharField(max_length=32)),
                ('last_name_th', models.CharField(max_length=32)),
                ('year', models.IntegerField()),
                ('status', models.CharField(max_length=16)),
                ('gpa', models.FloatField()),
                ('personal_id', models.CharField(max_length=32)),
                ('gender', models.CharField(max_length=16)),
                ('blood', models.CharField(max_length=4)),
                ('birth_date', models.IntegerField()),
                ('birth_month', models.IntegerField()),
                ('birth_year', models.IntegerField()),
                ('birth_province', models.CharField(max_length=32)),
                ('school_gpa', models.FloatField()),
                ('school_province', models.CharField(max_length=64)),
                ('school_id', models.CharField(max_length=32)),
                ('school_name_th', models.CharField(max_length=64)),
                ('school_name_en', models.CharField(max_length=64)),
                ('last_update', models.DateField(blank=True)),
                ('advisor', models.ForeignKey(to='students.Advisor')),
            ],
        ),
        migrations.AddField(
            model_name='program',
            name='program_id',
            field=models.CharField(default='', max_length=16),
            preserve_default=False,
        ),
    ]
