# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0004_course_prerequisite'),
    ]

    operations = [
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('level', models.CharField(max_length=32)),
                ('name_en', models.CharField(max_length=128)),
                ('name_th', models.CharField(max_length=128)),
                ('year', models.IntegerField(default=2555)),
            ],
        ),
        migrations.AddField(
            model_name='course',
            name='program',
            field=models.ForeignKey(default=None, to='students.Program'),
        ),
    ]
