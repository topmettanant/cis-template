# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0009_auto_20160618_1534'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='birth_date',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='birth_month',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='birth_province',
            field=models.CharField(null=True, max_length=32),
        ),
        migrations.AlterField(
            model_name='student',
            name='birth_year',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='blood',
            field=models.CharField(null=True, max_length=4),
        ),
        migrations.AlterField(
            model_name='student',
            name='gender',
            field=models.CharField(null=True, max_length=16),
        ),
        migrations.AlterField(
            model_name='student',
            name='personal_id',
            field=models.CharField(null=True, max_length=32),
        ),
    ]
