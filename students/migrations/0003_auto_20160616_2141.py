# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0002_auto_20160616_2140'),
    ]

    operations = [
        migrations.RenameField(
            model_name='course',
            old_name='courseid',
            new_name='course_id',
        ),
    ]
