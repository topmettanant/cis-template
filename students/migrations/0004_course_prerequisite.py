# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0003_auto_20160616_2141'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='prerequisite',
            field=models.ManyToManyField(to='students.Course'),
        ),
    ]
