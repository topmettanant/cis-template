# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0005_auto_20160617_0834'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourseGroup',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('unit_required', models.IntegerField(default=0)),
            ],
        ),
        migrations.RemoveField(
            model_name='course',
            name='program',
        ),
        migrations.AddField(
            model_name='coursegroup',
            name='courses',
            field=models.ManyToManyField(to='students.Course'),
        ),
        migrations.AddField(
            model_name='coursegroup',
            name='program',
            field=models.ForeignKey(to='students.Program'),
        ),
    ]
