# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0010_auto_20160618_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisor',
            name='is_updating',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='student',
            name='first_name_en',
            field=models.CharField(max_length=32, default=''),
        ),
        migrations.AlterField(
            model_name='student',
            name='first_name_th',
            field=models.CharField(max_length=32, default=''),
        ),
        migrations.AlterField(
            model_name='student',
            name='gpa',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='student',
            name='last_name_en',
            field=models.CharField(max_length=32, default=''),
        ),
        migrations.AlterField(
            model_name='student',
            name='last_name_th',
            field=models.CharField(max_length=32, default=''),
        ),
        migrations.AlterField(
            model_name='student',
            name='major_id',
            field=models.CharField(max_length=16, default=''),
        ),
        migrations.AlterField(
            model_name='student',
            name='status',
            field=models.CharField(max_length=16, default=''),
        ),
        migrations.AlterField(
            model_name='student',
            name='year',
            field=models.IntegerField(default=0),
        ),
    ]
