# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0007_auto_20160618_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisor',
            name='last_update',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='last_update',
            field=models.DateField(null=True),
        ),
    ]
