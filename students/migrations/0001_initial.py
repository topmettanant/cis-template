# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('courseid', models.CharField(max_length=32)),
                ('name_en', models.CharField(max_length=64)),
                ('name_th', models.CharField(max_length=64)),
                ('unit_lecture', models.IntegerField()),
                ('unit_lab', models.IntegerField()),
            ],
        ),
    ]
