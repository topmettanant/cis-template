# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0011_auto_20160618_1626'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='advisor',
            name='is_updating',
        ),
        migrations.AlterField(
            model_name='advisor',
            name='advisor_id',
            field=models.CharField(max_length=32, unique=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='student_id',
            field=models.CharField(max_length=32, unique=True),
        ),
    ]
