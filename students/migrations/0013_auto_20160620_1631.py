# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0012_auto_20160618_1637'),
    ]

    operations = [
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('section', models.IntegerField(null=True)),
                ('semester', models.IntegerField(null=True)),
                ('year', models.IntegerField(null=True)),
                ('registration_type', models.CharField(max_length=8, null=True)),
                ('grade', models.CharField(default='N', max_length=8)),
            ],
        ),
        migrations.AddField(
            model_name='course',
            name='year',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='student',
            name='program',
            field=models.ForeignKey(to='students.Program', null=True),
        ),
        migrations.AlterField(
            model_name='course',
            name='name_en',
            field=models.CharField(default='', max_length=64),
        ),
        migrations.AlterField(
            model_name='course',
            name='name_th',
            field=models.CharField(default='', max_length=64),
        ),
        migrations.AlterUniqueTogether(
            name='course',
            unique_together=set([('course_id', 'year')]),
        ),
        migrations.AlterUniqueTogether(
            name='program',
            unique_together=set([('year', 'program_id')]),
        ),
        migrations.AddField(
            model_name='registration',
            name='course',
            field=models.ForeignKey(to='students.Course'),
        ),
        migrations.AddField(
            model_name='registration',
            name='student',
            field=models.ForeignKey(to='students.Student'),
        ),
        migrations.AlterUniqueTogether(
            name='registration',
            unique_together=set([('student', 'course', 'semester', 'year')]),
        ),
    ]
