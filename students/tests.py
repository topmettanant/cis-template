from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from django.contrib.auth.models import User
from user_profile.models import UserDetails
from students.models import *
import time
import datetime


class StudentPermissionTest(StaticLiveServerTestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        super(StudentPermissionTest, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(StudentPermissionTest, self).tearDown()

    def add_google_user(self):
        self.driver.get(str(self.live_server_url))
        google_button = self.driver.find_element_by_xpath("//div[@view_id='google_button']")
        google_button.click()
        textbox = self.driver.switch_to.active_element
        textbox.send_keys("vacharapat@eng.src.ku.ac.th")
        textbox.send_keys(Keys.RETURN)
        time.sleep(1)
        textbox = self.driver.switch_to.active_element
        textbox.send_keys('toptop0869522249')
        textbox.send_keys(Keys.RETURN)

    def add_ku_user(self):
        self.driver.get(str(self.live_server_url))
        elem = self.driver.find_element_by_xpath("//input[@type='text']")
        elem.send_keys('sfengvcm')
        elem = self.driver.find_element_by_xpath("//input[@type='password']")
        elem.send_keys('top@KU0869522249')
        elem = self.driver.find_element_by_xpath("//div[@view_id='submit']")
        elem.click()

    def change_profile(self, email, first_name_th, last_name_th):
        self.driver.get(str(self.live_server_url) + '/setting/')
        edit_button = self.driver.find_element_by_xpath("//div[@view_id='edit-profile-button']")
        edit_button.click()
        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys(email)
        self.driver.find_element_by_xpath("//div[@webix_f_id='first_name_th']").click()
        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys(first_name_th)
        self.driver.find_element_by_xpath("//div[@webix_f_id='last_name_th']").click()
        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys(last_name_th)
        self.driver.find_element_by_xpath("//div[@webix_f_id='email']").click()
        save_button = self.driver.find_element_by_xpath("//button[@class='webixtype_form']")
        save_button.click()
        save_button.click()
        time.sleep(1)

    def test_public_access(self):
        self.driver.get(str(self.live_server_url)+'/students/')
        self.assertIn('Home',self.driver.title)
        self.assertIn('ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้', self.driver.page_source)

    def test_google_account_permission(self):
        self.add_google_user()
        self.driver.get(str(self.live_server_url))
        top = UserDetails.objects.all()[0]
        self.assertFalse(top.students_info)
        self.assertFalse(hasattr(top, 'advisor'))

        self.driver.get(str(self.live_server_url) + '/students/')
        self.assertIn('Home', self.driver.title)
        self.assertNotIn('ข้อมูลนิสิต', self.driver.page_source)

        self.change_profile('vacharapat@eng.src.ku.ac.th','วัชรพัฐ','เมตตาเมตตา')
        self.driver.get(str(self.live_server_url))
        top = UserDetails.objects.all()[0]
        self.assertFalse(top.students_info)
        self.assertFalse(hasattr(top, 'advisor'))

        self.driver.get(str(self.live_server_url) + '/students/')
        self.assertIn('Home', self.driver.title)
        self.assertNotIn('ข้อมูลนิสิต', self.driver.page_source)

        self.change_profile('vacharapat@eng.src.ku.ac.th','วัชรพัฐ','เมตตานันท')
        self.driver.get(str(self.live_server_url))
        top = UserDetails.objects.all()[0]
        self.assertTrue(top.students_info)
        self.assertTrue(hasattr(top, 'advisor'))

        adtop = top.advisor
        self.assertEqual(adtop.user_details, top)
        self.assertEqual(adtop.advisor_id, 'S3057')
        self.assertEqual(adtop.last_update, None)

        self.driver.get(str(self.live_server_url))
        self.assertIn('ข้อมูลนิสิต', self.driver.page_source)

    def test_ku_account_permission(self):
        self.add_ku_user()
        self.driver.get(str(self.live_server_url))
        top = UserDetails.objects.all()[0]
        self.assertTrue(top.students_info)
        self.assertTrue(hasattr(top, 'advisor'))

        adtop = top.advisor
        self.assertEqual(adtop.user_details, top)
        self.assertEqual(adtop.advisor_id, 'S3057')
        self.assertEqual(adtop.last_update, None)

        self.driver.get(str(self.live_server_url))
        self.assertIn('ข้อมูลนิสิต', self.driver.page_source)


class StudentInformationTest(StaticLiveServerTestCase):
    fixtures = ['general_courses.json','physical_courses.json','engineering_courses.json','cpe_courses.json','CPE2555.json']

    def setUp(self):
        self.driver = webdriver.Chrome()
        top = UserDetails.objects.create(first_name='Vacharapat',last_name='Mettanant')
        top.first_name_th = 'วัชรพัฐ'
        top.last_name_th = 'เมตตานันท'
        top.email = 'vacharapat@eng.src.ku.ac.th'
        top.students_info = True
        top.save()
        adtop = Advisor.objects.create(advisor_id='S3057', user_details=top)
        adtop.save()
        super(StudentInformationTest, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(StudentInformationTest, self).tearDown()

    def login_google(self):
        self.driver.get(str(self.live_server_url))
        google_button = self.driver.find_element_by_xpath("//div[@view_id='google_button']")
        google_button.click()
        textbox = self.driver.switch_to.active_element
        textbox.send_keys("vacharapat@eng.src.ku.ac.th")
        textbox.send_keys(Keys.RETURN)
        time.sleep(1)
        textbox = self.driver.switch_to.active_element
        textbox.send_keys('toptop0869522249')
        textbox.send_keys(Keys.RETURN)

    def test_fixtures(self):
        cs = Course.objects.all()
        self.assertTrue(len(cs) > 0)
        python = Course.objects.filter(course_id='03603111')[0]
        self.assertTrue(python.name_en == 'Programming Fundamentals I')
        cei55 = Program.objects.filter(program_id='T12',year=2555)[0]
        self.assertEqual(cei55.name_en, 'Computer Engineering and Informatics')
        elective_specific_courses = cei55.coursegroup_set.filter(name='elective_specific')[0].courses.all()
        ai = Course.objects.filter(course_id='03603461')[0]
        self.assertTrue(ai in elective_specific_courses)

    def test_get_students(self):
        self.login_google()
        self.driver.get(str(self.live_server_url) + '/students/')
        wait = WebDriverWait(self.driver, 120)
        elem = wait.until(EC.presence_of_element_located((By.XPATH,"//div[@view_id='gpa_diff']/div[@class='webix_view webix_chart']")))

        top = UserDetails.objects.all()[0].advisor
        self.assertEqual(top.last_update, datetime.date.today())
        students = Student.objects.all()
        self.assertTrue(len(students)>0)
        students = top.student_set.all()
        self.assertTrue(len(students)>0)
