from user_profile.models import UserDetails

def accessible_from(user_details):
    return user_details.students_info


def get_app_info():
    return {'url': '/students/', 'name': 'ข้อมูลนิสิต', 'icon':'graduation-cap'}
