from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^logout/$', 'home.views.user_logout', name='logout'),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url('', include('django.contrib.auth.urls', namespace='auth')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'home.views.index', name='index'),
    url(r'^login-ku/$', 'home.views.login_ku', name='login_ku'),
    url(r'^setting/', include('user_profile.urls')),
    url(r'^students/', include('students.urls')),
]
