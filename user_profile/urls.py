from django.conf.urls import include, url
from user_profile import views

urlpatterns = [

    url(r'^$', views.index, name='setting'),
    url(r'^general_profile/$', views.get_general_profile_json, name='general_profile'),
    url(r'^general_profile_panel/$', views.get_general_profile_panel, name='general_profile_panel'),
    url(r'^general_profile_form/$', views.get_general_profile_form, name='general_profile_form'),

]
