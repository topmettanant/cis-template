from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from django.contrib.auth.models import User
from user_profile.models import UserDetails
import time


class ProfileTest(StaticLiveServerTestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        super(ProfileTest, self).setUp()

    def add_google_user(self):
        self.driver.get(str(self.live_server_url))
        google_button = self.driver.find_element_by_xpath("//div[@view_id='google_button']")
        google_button.click()
        textbox = self.driver.switch_to.active_element
        textbox.send_keys("vacharapat@eng.src.ku.ac.th")
        textbox.send_keys(Keys.RETURN)
        time.sleep(1)
        textbox = self.driver.switch_to.active_element
        textbox.send_keys('toptop0869522249')
        textbox.send_keys(Keys.RETURN)


    def add_ku_user(self):
        self.driver.get(str(self.live_server_url))
        elem = self.driver.find_element_by_xpath("//input[@type='text']")
        elem.send_keys('sfengvcm')
        elem = self.driver.find_element_by_xpath("//input[@type='password']")
        elem.send_keys('top@KU0869522249')
        elem = self.driver.find_element_by_xpath("//div[@view_id='submit']")
        elem.click()


    def tearDown(self):
        self.driver.quit()
        super(ProfileTest, self).tearDown()


    def is_alert_visible(self):
        try:
            alert = self.driver.switch_to_alert()
            return alert
        except (NoAlertPresentException,):
            return False


    def test_login_required(self):
        self.driver.get(str(self.live_server_url) + '/setting/')
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertNotIn('Vacharapat', self.driver.page_source)
        self.assertIn('หน้าดังกล่าวไม่สามารถเข้าถึงได้', self.driver.page_source)

        self.driver.get(str(self.live_server_url) + '/setting/general_profile_panel/')
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertNotIn('Vacharapat', self.driver.page_source)
        self.assertIn('หน้าดังกล่าวไม่สามารถเข้าถึงได้', self.driver.page_source)

        self.driver.get(str(self.live_server_url) + '/setting/general_profile_form/')
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertNotIn('Vacharapat', self.driver.page_source)
        self.assertIn('หน้าดังกล่าวไม่สามารถเข้าถึงได้', self.driver.page_source)



    def test_google_profile_model(self):
        self.add_google_user()
        self.driver.get(str(self.live_server_url) + '/setting/')
        time.sleep(1)
        users = UserDetails.objects.all()
        self.assertTrue(len(users) == 1)
        top = users[0]
        self.assertTrue(top.first_name == 'Vacharapat')
        self.assertTrue(top.last_name == 'Mettanant')
        self.assertTrue(top.email == 'vacharapat@eng.src.ku.ac.th')
        self.assertTrue(top.first_name_th == '')
        self.assertTrue(top.last_name_th == '')
        self.assertTrue(top.internal_number == '')
        self.assertTrue(top.mobile_phone_number == '')

    def test_edit_profile(self):
        self.add_google_user()
        self.driver.get(str(self.live_server_url) + '/setting/')
        time.sleep(2)
        edit_button = self.driver.find_element_by_xpath("//div[@view_id='edit-profile-button']")
        edit_button.click()
        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys('vacharapat.ku.ac.th')
        save_button = self.driver.find_element_by_xpath("//button[@class='webixtype_form']")
        save_button.click()
        time.sleep(1)
        top = UserDetails.objects.all()[0]
        self.assertTrue(top.email == 'vacharapat@eng.src.ku.ac.th')

        self.driver.find_element_by_xpath("//div[@webix_f_id='email']").click()
        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys('vacharapat@eng.ku.ac.th')
        self.driver.find_element_by_xpath("//div[@webix_f_id='first_name_th']").click()
        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys('')
        save_button = self.driver.find_element_by_xpath("//button[@class='webixtype_form']")
        save_button.click()
        time.sleep(1)
        top = UserDetails.objects.all()[0]
        self.assertTrue(top.first_name_th == '')

        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys('วัชรพัฐ')
        self.driver.find_element_by_xpath("//div[@webix_f_id='last_name_th']").click()
        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys('')
        save_button = self.driver.find_element_by_xpath("//button[@class='webixtype_form']")
        save_button.click()
        time.sleep(1)
        top = UserDetails.objects.all()[0]
        self.assertTrue(top.last_name_th == '')

        text_box = self.driver.find_element_by_xpath("//input[@type='text']")
        text_box.send_keys('เมตตานันท')
        save_button = self.driver.find_element_by_xpath("//button[@class='webixtype_form']")
        save_button.click()
        save_button.click()
        time.sleep(1)

        top = UserDetails.objects.all()[0]
        self.assertTrue(top.first_name_th == 'วัชรพัฐ')
        self.assertTrue(top.last_name_th == 'เมตตานันท')


    def test_show_ku_profile(self):
        self.add_ku_user()
        self.driver.get(str(self.live_server_url) + '/setting/')
        time.sleep(1)
        self.assertIn('วัชรพัฐ', self.driver.page_source)
        users = UserDetails.objects.all()
        self.assertTrue(len(users) == 1)
        top = users[0]
        self.assertTrue(top.first_name == 'Vacharapat')
        self.assertTrue(top.last_name == 'Mettanant')
        self.assertTrue(top.first_name_th == 'วัชรพัฐ')
        self.assertTrue(top.last_name_th == 'เมตตานันท')
        self.assertTrue(top.email == '')
        self.assertTrue(top.internal_number == '')
        self.assertTrue(top.mobile_phone_number == '')

    def test_update_profile_from_ku(self):
        self.add_google_user()
        self.driver.get(str(self.live_server_url) + '/logout/')
        time.sleep(1)
        self.add_ku_user()
        users = UserDetails.objects.all()
        self.assertTrue(len(users) == 1)
        top = users[0]
        self.assertTrue(top.email == 'vacharapat@eng.src.ku.ac.th')
        self.assertTrue(top.first_name_th == 'วัชรพัฐ')
        self.assertTrue(top.last_name_th == 'เมตตานันท')

    def test_update_profile_from_google(self):
        self.add_ku_user()
        self.driver.get(str(self.live_server_url) + '/logout/')
        time.sleep(1)
        top = UserDetails.objects.all()[0]
        self.add_google_user()
        self.driver.get(str(self.live_server_url))
        users = UserDetails.objects.all()
        self.assertTrue(len(users) == 1)
        top = users[0]
        self.assertTrue(top.email == 'vacharapat@eng.src.ku.ac.th')
        self.assertTrue(top.first_name_th == 'วัชรพัฐ')
        self.assertTrue(top.last_name_th == 'เมตตานันท')
