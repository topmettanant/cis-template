from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from user_profile.models import UserDetails
from django.contrib.auth.decorators import login_required
from user_profile.forms import UserProfileForm
from django.http import HttpResponse
import json
from django.forms.models import model_to_dict
from students.views import get_advisor_from


def get_user_details(request):
    user = request.user
    user_details = UserDetails.objects.get(first_name=user.first_name, last_name=user.last_name)
    return user_details


@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def index(request):
    user_details = get_user_details(request)
    context = {'request': request, 'user_details': user_details}
    return render(request, 'user_profile/index.html', context)


@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_general_profile_json(request):
    user_details = get_user_details(request)
    if request.method == 'POST':
        user_details.email = request.POST.get("email","")
        user_details.first_name_th = request.POST.get("first_name_th","")
        user_details.last_name_th = request.POST.get("last_name_th","")
        user_details.internal_number = request.POST.get("internal_number","")
        user_details.mobile_phone_number = request.POST.get("mobile_phone_number","")
        user_details.save()
        advisor = get_advisor_from(user_details)
        if advisor != None:
            user_details.students_info = True
            user_details.save()
    return HttpResponse(json.dumps(model_to_dict(user_details)), content_type="application/json")


@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_general_profile_panel(request):
    user_details = get_user_details(request)
    context = {'request': request, 'user_details': user_details}
    return render(request, 'user_profile/general_profile_panel.html', context)


@login_required(login_url='/?error=ขออภัย หน้าดังกล่าวไม่สามารถเข้าถึงได้')
def get_general_profile_form(request):
    user_details = get_user_details(request)
    form = UserProfileForm(instance=user_details)

    context = {'form': form, 'user_details': user_details}
    return render(request, 'user_profile/general_profile_form.html', context)
