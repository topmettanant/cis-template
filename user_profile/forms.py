from django import forms
from user_profile.models import UserDetails

class UserProfileForm(forms.ModelForm):
    email = forms.EmailField(max_length=70, label="อีเมล",
                             widget=forms.TextInput(attrs={'size': 30}),
                             error_messages={'required': 'กรุณากรอกอีเมล','invalid': 'กรุณากรอกอีเมลให้ถูกต้อง'})
    first_name_th = forms.CharField(max_length=32, label="ชื่อจริง(ไทย)",
                                    widget=forms.TextInput(attrs={'size': 30}),
                                    error_messages={'required':'กรุณากรอกชื่อจริง','invalid': 'กรุณากรอกชื่อจริงให้ถูกต้อง'})
    last_name_th = forms.CharField(max_length=32, label="นามสกุล(ไทย)",
                                   widget=forms.TextInput(attrs={'size': 30}),
                                   error_messages={'required':'กรุณากรอกนามสกุล','invalid':'กรุณากรอกนามสกุลให้ถูกต้อง'})
    internal_number = forms.CharField(max_length=16, label="หมายเลขภายใน", required=False, widget=forms.TextInput(attrs={'size': 30}))
    mobile_phone_number = forms.CharField(max_length=16, label="โทรศัพท์มือถือ", required=False, widget=forms.TextInput(attrs={'size': 30}))

    class Meta:
        model = UserDetails
        fields = ('email', 'first_name_th','last_name_th', 'internal_number', 'mobile_phone_number')
