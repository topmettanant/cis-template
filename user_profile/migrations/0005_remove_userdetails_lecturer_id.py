# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0004_userdetails_students_info'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdetails',
            name='lecturer_id',
        ),
    ]
