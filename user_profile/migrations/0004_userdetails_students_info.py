# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0003_userdetails_lecturer_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdetails',
            name='students_info',
            field=models.BooleanField(default=False),
        ),
    ]
