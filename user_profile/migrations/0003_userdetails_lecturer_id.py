# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0002_auto_20160602_0956'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdetails',
            name='lecturer_id',
            field=models.CharField(max_length=16, default=''),
            preserve_default=False,
        ),
    ]
