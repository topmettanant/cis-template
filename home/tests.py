from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from django.contrib.auth.models import User
from user_profile.models import UserDetails
import time

class HomeLoginTest(StaticLiveServerTestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(str(self.live_server_url))
        super(HomeLoginTest, self).setUp()


    def tearDown(self):
        self.driver.quit()
        super(HomeLoginTest, self).tearDown()


    def is_alert_visible(self):
        try:
            alert = self.driver.switch_to_alert()
            return alert
        except (NoAlertPresentException,):
            return False


    def test_index_view_global(self):
        self.assertIn('CIS - Home', self.driver.title)
        self.assertIn("เข้าสู่ระบบด้วย Google", self.driver.page_source)
        self.assertIn("บัญชีนนทรี", self.driver.page_source)
        self.assertIn("รหัสผ่าน", self.driver.page_source)
        self.assertNotIn('เมนู', self.driver.page_source)


    def test_wrong_login_google(self):
        google_button = self.driver.find_element_by_xpath("//div[@view_id='google_button']")
        google_button.click()
        textbox = self.driver.switch_to.active_element
        textbox.send_keys("vacharapat@gmail.com")
        textbox.send_keys(Keys.RETURN)
        time.sleep(1)
        textbox = self.driver.switch_to.active_element
        textbox.send_keys('wrongworngpassword')
        textbox.send_keys(Keys.RETURN)
        time.sleep(1)
        self.assertIn("The email and password you entered don't match.", self.driver.page_source)
        textbox = self.driver.switch_to.active_element
        textbox.send_keys('toptop0869522249')
        textbox.send_keys(Keys.RETURN)
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertIn('เข้าสู่ระบบ', self.driver.page_source)
        self.assertIn('บัญชี Google ดังกล่าวไม่สามารถใช้งานได้', self.driver.page_source)


    def test_first_login_google(self):
        google_button = self.driver.find_element_by_xpath("//div[@view_id='google_button']")
        google_button.click()
        textbox = self.driver.switch_to.active_element
        textbox.send_keys("vacharapat@eng.src.ku.ac.th")
        textbox.send_keys(Keys.RETURN)
        time.sleep(1)
        textbox = self.driver.switch_to.active_element
        textbox.send_keys('toptop0869522249')
        textbox.send_keys(Keys.RETURN)
        self.assertIn('Setting', self.driver.title)
        self.assertIn('Vacharapat', self.driver.page_source)
        self.assertIn('เมนู', self.driver.page_source)
        user_details = UserDetails.objects.all()
        self.assertTrue(len(user_details) == 1)


    def test_wrong_user_ku(self):
        login_button = self.driver.find_element_by_xpath("//div[@view_id='submit']")
        login_button.click()
        time.sleep(1)
        self.assertIn('กรุณากรอกชื่อบัญชี', self.driver.page_source)

        elem = self.driver.find_element_by_xpath("//input[@type='text']")
        elem.send_keys('sfengvcmm')
        login_button.click()
        time.sleep(1)
        self.assertIn('กรุณากรอกรหัสผ่าน', self.driver.page_source)
        elem = self.driver.find_element_by_xpath("//input[@type='password']")
        elem.send_keys('wrongpassword')
        login_button.click()
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertIn('เข้าสู่ระบบ', self.driver.page_source)
        self.assertIn('บัญชีหรือรหัสผ่านไม่ถูกต้อง', self.driver.page_source)

    def test_wrong_password_ku(self):
        elem = self.driver.find_element_by_xpath("//input[@type='text']")
        elem.send_keys('sfengvcm')
        elem = self.driver.find_element_by_xpath("//input[@type='password']")
        elem.send_keys('wrongpassword')
        elem = self.driver.find_element_by_xpath("//div[@view_id='submit']")
        elem.click()
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertIn('เข้าสู่ระบบ', self.driver.page_source)
        time.sleep(1)
        self.assertIn('บัญชีหรือรหัสผ่านไม่ถูกต้อง', self.driver.page_source)

    def test_first_login_ku(self):
        elem = self.driver.find_element_by_xpath("//input[@type='text']")
        elem.send_keys('sfengvcm')
        elem = self.driver.find_element_by_xpath("//input[@type='password']")
        elem.send_keys('top@KU0869522249')
        elem = self.driver.find_element_by_xpath("//div[@view_id='submit']")
        elem.click()
        self.assertIn('Setting', self.driver.title)
        self.assertIn('Vacharapat', self.driver.page_source)
        self.assertIn('วัชรพัฐ', self.driver.page_source)
        self.assertIn('เมนู', self.driver.page_source)
        user_details = UserDetails.objects.all()
        self.assertTrue(len(user_details) == 1)

    def test_more_login_google(self):
        user_details = UserDetails.objects.get_or_create(first_name='Vacharapat', last_name='Mettanant')[0]
        user_details.save()

        google_button = self.driver.find_element_by_xpath("//div[@view_id='google_button']")
        google_button.click()
        textbox = self.driver.switch_to.active_element
        textbox.send_keys("vacharapat@eng.src.ku.ac.th")
        textbox.send_keys(Keys.RETURN)
        time.sleep(1)
        textbox = self.driver.switch_to.active_element
        textbox.send_keys('toptop0869522249')
        textbox.send_keys(Keys.RETURN)
        self.assertIn('Home', self.driver.title)
        self.assertIn('เมนู', self.driver.page_source)
        user_details = UserDetails.objects.all()
        vacharapat = user_details[0]
        self.assertTrue(vacharapat.email == 'vacharapat@eng.src.ku.ac.th')
        self.assertTrue(len(user_details) == 1)

        elem = self.driver.find_element_by_xpath("//div[@view_id='account-button']")
        elem.click()
        wait = WebDriverWait(self.driver, 10)
        elem = wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, "ออกจากระบบ")))
        elem.click()
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertIn('เข้าสู่ระบบ', self.driver.page_source)

    def test_more_login_ku(self):
        user_details = UserDetails.objects.get_or_create(first_name='Vacharapat', last_name='Mettanant')[0]
        user_details.save()

        elem = self.driver.find_element_by_xpath("//input[@type='text']")
        elem.send_keys('sfengvcm')
        elem = self.driver.find_element_by_xpath("//input[@type='password']")
        elem.send_keys('top@KU0869522249')
        elem = self.driver.find_element_by_xpath("//div[@view_id='submit']")
        elem.click()
        self.assertIn('Home', self.driver.title)
        self.assertNotIn('เข้าสู่ระบบ', self.driver.page_source)
        self.assertIn('เมนู', self.driver.page_source)
        user_details = UserDetails.objects.all()
        vacharapat = user_details[0]
        self.assertTrue(vacharapat.first_name_th == 'วัชรพัฐ')
        self.assertTrue(vacharapat.last_name_th == 'เมตตานันท')
        self.assertTrue(len(user_details) == 1)

        elem = self.driver.find_element_by_xpath("//div[@view_id='account-button']")
        elem.click()
        wait = WebDriverWait(self.driver, 10)
        elem = wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, "ออกจากระบบ")))
        elem.click()
        self.assertIn('Home', self.driver.title)
        self.assertIn('Central Information System', self.driver.page_source)
        self.assertIn('เข้าสู่ระบบ', self.driver.page_source)
