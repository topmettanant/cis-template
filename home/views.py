# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from social.exceptions import AuthForbidden
from user_profile.models import UserDetails
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
import urllib, urllib.request
from students.views import get_advisor_from


def index(request):
    user = request.user
    error = request.GET.get('error', None)
    if user and not user.is_anonymous():
        user_details, created = UserDetails.objects.get_or_create(first_name=user.first_name, last_name=user.last_name)
        if user_details.email == '' or user_details.email == None:
            user_details.email = user.email
            user_details.save()
        if created:
            return HttpResponseRedirect('/setting/')
        else:
            context = {'request': request, 'user_details': user_details, 'error': error}
            return render(request, 'home/index.html', context)
    else:
        return render(request, 'home/login.html', {'request': request, 'error': error})


def email_check(strategy, details, *args, **kwargs):
    mail = str(details.get('email'))
    if not mail.endswith('@eng.src.ku.ac.th'):
        return HttpResponseRedirect('/?error=ขออภัย บัญชี Google ดังกล่าวไม่สามารถใช้งานได้')
        #raise AuthForbidden(kwargs.get('backend'))


def get_profile(html):
    i = html.find('LDAP Authentication recognises and greets:')
    if i<0:
        return None
    i = html.find('>', i)
    j = html.find('<', i)
    name = html[i+1: j].strip()
    first_name, last_name = tuple(name.split(' '))

    i = html.find('Entrydn:')
    i = html.find('ou=', i)
    i = html.find('=', i)
    j = html.find(',', i)
    position = html[i+1: j].strip()

    i = html.find('Thainame')
    i = html.find('>', i)
    j = html.find('<', i)
    name_th = html[i+1: j].strip()
    first_name_th, last_name_th = tuple(name_th.split(' '))

    i = html.find('User ID:')
    if i<0:
        i = html.find('Account:')
    i = html.find('>', i)
    j = html.find('<', i)
    userid = html[i+1: j].strip()

    i = html.find('Faculty:')
    i = html.find('>', i)
    j = html.find('<', i)
    faculty = html[i+1: j].strip()
    return {'first_name': first_name, 'last_name': last_name, 'position': position,
            'first_name_th': first_name_th, 'last_name_th': last_name_th,
            'userid': userid, 'faculty': faculty}


def login_ku(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        data = urllib.parse.urlencode({'userid': username, 'password': password})
        try:
            r = urllib.request.urlopen('https://ldap.src.ku.ac.th/ldap_html/ldap/authtest.php', data.encode('utf-8')).readall().decode('utf-8')
            r = str(r)
            if 'user not authenticated' in r or 'User unknown' in r:
                return HttpResponseRedirect('/?error=ขออภัย บัญชีหรือรหัสผ่านไม่ถูกต้อง')
            else:
                profile = get_profile(r)
                if profile == None:
                    raise TypeError
        except:
            try:
                data = urllib.parse.urlencode({'userid': username, 'password': password, 'ldap_host':'ldap.src.ku.ac.th','base_dn':'ou=src,dc=ku,dc=ac,dc=th'})
                r = urllib.request.urlopen('https://ldap2.ku.ac.th/ldap_html/ldap/authtest.php', data.encode('utf-8')).readall().decode('utf-8')
                r = str(r)
                if 'user not authenticated' in r or 'User unknown' in r:
                    return HttpResponseRedirect('/?error=ขออภัย บัญชีหรือรหัสผ่านไม่ถูกต้อง')
                else:
                    profile = get_profile(r)
                    if profile == None:
                        raise TypeError
            except:
                return HttpResponseRedirect('/?error=ขออภัย ไม่สามารถเชื่อมต่อระบบบัญชีนนทรีได้ โปรดรอการแก้ไข หรือเข้าสู่ระบบด้วยบัญชี Google')

        if profile['position'] != 'student' and profile['faculty'] == 'คณะวิศวกรรมศาสตร์ศรีราชา':
            user, created = User.objects.get_or_create(username=profile['userid'])
            user.set_password(password)
            user.save()
            user = authenticate(username=profile['userid'], password=password)
            if created:
                user.first_name = profile['first_name']
                user.last_name = profile['last_name']
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                user.save()
            login(request, user)
            user_details, created = UserDetails.objects.get_or_create(first_name=profile['first_name'], last_name=profile['last_name'])
            if user_details.first_name_th == '':
                user_details.first_name_th = profile['first_name_th']
            if user_details.last_name_th == '':
                user_details.last_name_th = profile['last_name_th']
            user_details.save()
            if created:
                advisor = get_advisor_from(user_details)
                if advisor != None:
                    user_details.students_info = True
                    user_details.save()
                return HttpResponseRedirect('/setting/?message=ในขั้นตอนแรก กรุณาแก้ไขข้อมูลให้เป็นปัจจุบัน')
            else:
                return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')


def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')
